# README #

Qt app that acts as a native container for Udemy.com web app for convenience and extra features.

1. Log in securely saved, just like other native apps do.
2. Convenient and direct access to course taking.
3. Put the windows always on top and change opacity and take Udemy courses while doing something else on your desktop!

I stopped the development, losing further interests. Originally, this was a personal project expressing a native application idea.

The project won't compile as a small dynamic library has been removed for security.

The project may be useful for someone tries to build a Qt app that requires Restful API interaction with fully customized application window. I leave the code open for reference.