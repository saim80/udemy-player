#include "coursebrowser.h"
#include "ui_coursebrowser.h"

#include "hoverbutton.h"
#include "settings.h"
#include "signinform.h"
#include "udemyapiaccessmanager.h"

#include <QPainter>
#include <QtWebKit>
#include <QJsonObject>

CourseBrowser::CourseBrowser(QWidget *parent) :
    Window(parent),
    ui(new Ui::CourseBrowser)
{
    setWindowFlags(Qt::Window|Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);
    
    ui->setupUi(this);
    ui->webView->setContextMenuPolicy(Qt::NoContextMenu);
    ui->webView->setRenderHints(
                                QPainter::Antialiasing |
                                QPainter::HighQualityAntialiasing |
                                QPainter::NonCosmeticDefaultPen |
                                QPainter::SmoothPixmapTransform |
                                QPainter::TextAntialiasing
                                );
    
    netManager = new UdemyWebAccessManager(this);
    
    resizeEnabled = true;
    setMouseTracking(true);
    setAttribute(Qt::WA_Hover);
    
    connect(ui->bar->signOutButton, &QPushButton::released, [=]() {
        Settings::secureSet(SignInForm::KeyEmail, "");
        Settings::secureSet(SignInForm::KeyPassword, "");
        
        close();
        
        auto signInForm = new SignInForm();
        
        signInForm->show();
    });
    
    connect(ui->bar->closeButton, &HoverButton::clicked, [=]() {
        QApplication::quit();
    });
    
    connect(ui->courseList, &CourseList::itemClicked, [=](const QVariantMap &map) {
        openCourse(map);
    });
    
    
    QNetworkCookieJar *cookieJar = new QNetworkCookieJar(this);
    netManager->setCookieJar(cookieJar);
    
    ui->webView->settings()->setAttribute(QWebSettings::PluginsEnabled, false);
    ui->webView->settings()->setAttribute(QWebSettings::JavaEnabled, false);
    ui->webView->settings()->setAttribute(QWebSettings::JavascriptEnabled, true);
    ui->webView->settings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);
    ui->webView->settings()->setAttribute(QWebSettings::LocalStorageDatabaseEnabled, true);
    ui->webView->settings()->setAttribute(QWebSettings::OfflineWebApplicationCacheEnabled, true);
    ui->webView->settings()->setAttribute(QWebSettings::OfflineStorageDatabaseEnabled, true);
    
    
    QWebPage *newWeb = ui->webView->page();
    
    newWeb->setNetworkAccessManager(netManager);
    newWeb->setForwardUnsupportedContent(true);
}

CourseBrowser::~CourseBrowser()
{
    delete ui;
}

void CourseBrowser::openCourse(const QVariantMap &map)
{
    const auto courseURL = Settings::getUrl(QString("%1/learn/v4/").arg(map["published_title"].toString()));
    
    qDebug() << courseURL;
    
    ui->webView->setUrl(courseURL);
    
}
