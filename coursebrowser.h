#ifndef COURSEBROWSER_H
#define COURSEBROWSER_H

#include "window.h"
#include "titlebar.h"

namespace Ui {
class CourseBrowser;
}


class CourseBrowser : public Window
{
    Q_OBJECT
public:
    explicit CourseBrowser(QWidget *parent = 0);
    ~CourseBrowser();
    
    void openCourse(const QVariantMap &map);
private:
    Ui::CourseBrowser *ui;
signals:

public slots:
private slots:
};

#endif // COURSEBROWSER_H
