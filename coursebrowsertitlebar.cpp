#include "coursebrowsertitlebar.h"
#include <QPushButton>
#include <QHBoxLayout>

CourseBrowserTitleBar::CourseBrowserTitleBar(QWidget *parent) : TitleBar(parent)
{
    configureSignOutButton();
}

void CourseBrowserTitleBar::configureSignOutButton() {
    auto const hlayout = qobject_cast<QHBoxLayout*>(layout());

    signOutButton = new QPushButton(this);

    signOutButton->setText(tr("Sign Out"));
    signOutButton->setStyleSheet("color: white; padding: 4px 8px; margin-right:4px; border-width:1px; border-color:black;");

#ifdef Q_OS_MAC
    hlayout->addWidget(signOutButton);
#else
    hlayout->insertWidget(hlayout->count()-2, signOutButton);
#endif
}
