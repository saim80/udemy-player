#ifndef COURSEBROWSERTITLEBAR_H
#define COURSEBROWSERTITLEBAR_H

#include <QtGui>
#include "titlebar.h"

class CourseBrowserTitleBar : public TitleBar
{
    Q_OBJECT
public:
    explicit CourseBrowserTitleBar(QWidget *parent = 0);

    class QPushButton *signOutButton;

private:
    void configureSignOutButton();
};

#endif // COURSEBROWSERTITLEBAR_H
