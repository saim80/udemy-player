#include "courselist.h"
#include "waitingspinnerwidget.h"

#include <QVBoxLayout>
#include <QListWidget>
#include <QtDebug>
#include <QPixmapCache>
#include <QPainter>
#include <QIcon>
#include <QGraphicsOpacityEffect>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QScrollBar>

//--------------------------------------------------------------------------------------------------------------------------------

const int CourseListItemDelegate::ItemHeight = 144;

CourseListItemDelegate::CourseListItemDelegate(QWidget *parent) : QAbstractItemDelegate(parent)
{
}

void CourseListItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    const auto parentWidget = qobject_cast<CourseList*>(parent());
    const auto listWidget   = parentWidget->_listWidget;
    const auto item         = listWidget->item(index.row());
    const auto itemData     = index.data().toMap();
    
    auto itemGeometry = option.rect;
    auto titleGeometry = itemGeometry.marginsRemoved(QMargins(5, 5, 5, 5));
    auto biggerFont = option.font;
    
    biggerFont.setBold(true);
    
    QPen blackPen(QColor("black"));
    QPen whitePen(QColor("white"));
    QBrush bgBrush;
    
    auto bgImage = item->icon().pixmap(itemGeometry.size()).toImage();
    
    painter->eraseRect(itemGeometry);
    painter->setOpacity(item->isSelected() ? 0.8 : 0.45);
    painter->drawImage(itemGeometry, bgImage);
    if (item->isSelected()) {
        painter->setOpacity(0.5);
        painter->fillRect(itemGeometry, Qt::black);
    }
    painter->setOpacity(1);
    painter->setPen(whitePen);
    if (item->isSelected()) {
        painter->setFont(biggerFont);
        painter->drawText(titleGeometry, Qt::AlignLeft | Qt::AlignVCenter | Qt::TextWordWrap, item->text());
    } else {
        painter->setFont(option.font);
        painter->drawText(titleGeometry, Qt::AlignLeft | Qt::AlignBottom | Qt::TextWordWrap, item->text());
    }
    painter->setOpacity(0.4);
    painter->setPen(blackPen);
    painter->drawLine(QLine(itemGeometry.bottomLeft(), itemGeometry.bottomRight()));
    painter->setOpacity(1.0);
}

QSize CourseListItemDelegate::sizeHint(const QStyleOptionViewItem &, const QModelIndex &) const
{
    const auto parentWidget = qobject_cast<CourseList*>(parent());
    
    return QSize(parentWidget->width(), ItemHeight);
}

//--------------------------------------------------------------------------------------------------------------------------------

CourseList::CourseList(QWidget *parent) :
    QWidget(parent),
    PaginatedData(),
    netManager(new UdemyAPIAccessManager(this)),
    spinner(new WaitingSpinnerWidget(this)),
    _previouslySelected(nullptr),
    _currentReply(nullptr),
    _listWidget(new QListWidget(this)),
    _listLayout(new QVBoxLayout(this)),
    _itemDelegate(new CourseListItemDelegate(this))
{
    auto font = QFont("HelveticaNeue");
    
    font.setPointSize(18);
    
    _listLayout->addWidget(_listWidget);
    _listLayout->setMargin(0);
    _listWidget->setFocusPolicy(Qt::NoFocus);
    _listWidget->setItemDelegate(_itemDelegate);
    _listWidget->setFont(font);
    
    auto const verticalScrollBar = _listWidget->verticalScrollBar();
    
    connect(verticalScrollBar, &QScrollBar::valueChanged, [=] (int value) {
        if (value == verticalScrollBar->maximum()) {
            loadNext();
        }
    });
    
    connect(_listWidget, &QListWidget::itemClicked, [=] (QListWidgetItem *item) {
        _selectItem(item);
    });
    
    loadNext();
}

CourseList::~CourseList()
{
}

void CourseList::showLoading()
{
    spinner->start();
}

void CourseList::hideLoading()
{
    spinner->stop();
}

void CourseList::_selectItem(QListWidgetItem *item)
{
    if (_previouslySelected != item) {
        if (_previouslySelected) {
            _previouslySelected->setSelected(false);
        }
        _previouslySelected = item;
        item->setSelected(true);
        emit itemClicked(item->data(Qt::UserRole).toMap());
    }
}

void CourseList::reset()
{
    PaginatedData::reset();
    
    _courseDataList.clear();
    _listWidget->reset();
}

void CourseList::load(const int& page, const int& pageSize, UdemyAPI::JSONCompletion completion)
{
    if (isLoading()) {
        _currentReply->abort();
        _currentReply = nullptr;
    }
    
    auto params = QVariantMap();
    
    params.insert("page", page);
    params.insert("page_size", pageSize);
    
    auto reply = netManager->get("users/me/subscribed-courses", params);
    
    if (page == 1)
    {
        showLoading();
    }
    
    _currentReply = reply;
    
    QObject::connect(reply, &QNetworkReply::finished, [=]() {
        const auto responseDoc = QJsonDocument::fromJson(reply->readAll());
        const auto resultMap   = responseDoc.toVariant().toMap();
        
        if (resultMap.size() > 0)
        {
            auto newList = resultMap["results"].toList();
            
            nextURL = resultMap["next"].toString();
            
            _courseDataList.append(newList);
            
            updateUI(newList);
        }
        
        _currentReply = nullptr;
        
        if (completion)
        {
            completion(responseDoc);
        }
        
        reply->deleteLater();
        
        if (page == 1)
        {
            auto const item = this->_listWidget->item(0);
            if (item) {
                _selectItem(item);
            }
            hideLoading();
        }
    });
}

void CourseList::updateUI(const QVariantList &appended)
{
    for (auto it = appended.constBegin(); it != appended.constEnd(); it++)
    {
        auto item = new QListWidgetItem(_listWidget);
        auto data = it->toMap();
        
        item->setData(Qt::UserRole, data);
        item->setText(data["title"].toString());
        
        _listWidget->addItem(item);
        
        downloadImageFromURL(QUrl(data["image_480x270"].toString()), item, nullptr);
    }
}

void CourseList::downloadImageFromURL(const QUrl& URL, QListWidgetItem* item,
                                      std::function<void()> completion) const
{
    QPixmap loadedPixmap;
    
    if (QPixmapCache::find(item->text(), loadedPixmap))
    {
        if (completion)
        {
            item->setIcon(QIcon(loadedPixmap));
            completion();
        }
    }
    else
    {
        auto reply = netManager->QNetworkAccessManager::get(QNetworkRequest(URL));
        
        QObject::connect(reply, &QNetworkReply::finished, [=] () {
            auto const data = reply->readAll();
            auto pixmap = QPixmap();
            
            if (pixmap.loadFromData(data)) {
                QPixmapCache::insert(item->text(), pixmap);
                
                item->setIcon(QIcon(pixmap));
            }
            if (completion)
            {
                completion();
            }
        });
    }
}
