#ifndef COURSELIST_H
#define COURSELIST_H

#include <QWidget>
#include <QAbstractItemDelegate>

#include "paginateddata.h"

//--------------------------------------------------------------------------------------------------------------------------------

class CourseListItemDelegate : public QAbstractItemDelegate
{
    Q_OBJECT
public:
    CourseListItemDelegate(QWidget *parent = 0);
    
    static const int ItemHeight;
    
    void paint ( QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const override;
    QSize sizeHint ( const QStyleOptionViewItem & option, const QModelIndex & index ) const override;
private:
};

//--------------------------------------------------------------------------------------------------------------------------------

class CourseList : public QWidget, public PaginatedData
{
    Q_OBJECT

public:
    explicit CourseList(QWidget *parent = 0);
    ~CourseList();

public:
    UdemyAPIAccessManager *netManager;
    class WaitingSpinnerWidget *spinner;
    
    virtual void reset() override;
protected:
    void showLoading();
    void hideLoading();
    
    inline bool isLoading() { return _currentReply != nullptr; };
    
    virtual void load(const int& page, const int& pageSize, UdemyAPI::JSONCompletion completion) override;
    void downloadImageFromURL(const QUrl& URL, class QListWidgetItem* item,
                              std::function<void()> completion) const;
    
    void updateUI(const QVariantList &appended);
private:
    class QListWidgetItem *_previouslySelected;
    class QNetworkReply *_currentReply;
    class QListWidget* _listWidget;
    class QVBoxLayout* _listLayout;
    QVariantList _courseDataList;
    CourseListItemDelegate* _itemDelegate;
    
    friend CourseListItemDelegate;
    
    void _selectItem(QListWidgetItem *item);
signals:
    void itemClicked(const QVariantMap &itemData);
};

#endif // COURSELIST_H
