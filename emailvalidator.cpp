#include "emailvalidator.h"

EmailValidator::EmailValidator(QObject *parent) : QRegExpValidator(parent)
{
    QRegExp rx("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b");

    rx.setCaseSensitivity(Qt::CaseInsensitive);
    rx.setPatternSyntax(QRegExp::RegExp);

    setRegExp(rx);
}

QValidator::State EmailValidator::validate(QString& input, int& pos) const
{
    if (input.size() == 0)
    {
        return QValidator::State::Acceptable;
    }

    return QRegExpValidator::validate(input, pos);
}
