#ifndef EMAILVALIDATOR_H
#define EMAILVALIDATOR_H

#include <QRegExpValidator>

class EmailValidator : public QRegExpValidator
{
    Q_OBJECT
public:
    explicit EmailValidator(QObject *parent=0);

    virtual QValidator::State validate(QString& input, int& pos) const Q_DECL_OVERRIDE;
};

#endif // EMAILVALIDATOR_H
