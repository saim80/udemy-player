#include "hoverbutton.h"

#include <QWidget>
#include <QEvent>

HoverButton::HoverButton(QWidget *parent) : QPushButton(parent), _didEnter(false)
{
    setCheckable(true);
}

void HoverButton::enterEvent(class QEvent *)
{
    _didEnter = true;
    _updateImage();
}

void HoverButton::leaveEvent(class QEvent *)
{
    _didEnter = false;
    _updateImage();
}


void HoverButton::setNormalImagePath(const QString &path)
{
    normalImagePath = path;
    if (!_didEnter)
    {
        _updateImage();
    }
}

void HoverButton::setHoverImagePath(const QString &path)
{
    hoverImagePath = path;
    if (_didEnter)
    {
        _updateImage();
    }
}

void HoverButton::_updateImage()
{
    if (_didEnter)
    {
        setStyleSheet("border-width:0px; qproperty-icon:url("+hoverImagePath+");");
    }
    else
    {
        setStyleSheet("border-width:0px; qproperty-icon:url("+normalImagePath+");");
    }
}