#ifndef HOVERBUTTON_H
#define HOVERBUTTON_H

#include <QPushButton>

class HoverButton : public QPushButton
{
    Q_OBJECT
public:
    HoverButton(class QWidget *parent = 0);

    void setNormalImagePath(const QString &path);
    void setHoverImagePath(const QString &path);
protected:
    QString normalImagePath;
    QString hoverImagePath;

    void enterEvent(class QEvent *event) override;
    void leaveEvent(class QEvent *event) override;
    
private:
    bool _didEnter;
    void _updateImage();
};

#endif // HOVERBUTTON_H
