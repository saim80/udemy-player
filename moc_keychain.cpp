/****************************************************************************
** Meta object code from reading C++ file 'keychain.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Libraries/qt-keychain/keychain.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'keychain.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QKeychain__Job_t {
    QByteArrayData data[5];
    char stringdata0[49];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QKeychain__Job_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QKeychain__Job_t qt_meta_stringdata_QKeychain__Job = {
    {
QT_MOC_LITERAL(0, 0, 14), // "QKeychain::Job"
QT_MOC_LITERAL(1, 15, 8), // "finished"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 15), // "QKeychain::Job*"
QT_MOC_LITERAL(4, 41, 7) // "doStart"

    },
    "QKeychain::Job\0finished\0\0QKeychain::Job*\0"
    "doStart"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QKeychain__Job[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   24,    2, 0x06 /* Public */,

 // methods: name, argc, parameters, tag, flags
       4,    0,   27,    2, 0x01 /* Protected */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,

 // methods: parameters
    QMetaType::Void,

       0        // eod
};

void QKeychain::Job::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Job *_t = static_cast<Job *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->finished((*reinterpret_cast< QKeychain::Job*(*)>(_a[1]))); break;
        case 1: _t->doStart(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QKeychain::Job* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Job::*_t)(QKeychain::Job * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Job::finished)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject QKeychain::Job::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QKeychain__Job.data,
      qt_meta_data_QKeychain__Job,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QKeychain::Job::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QKeychain::Job::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QKeychain__Job.stringdata0))
        return static_cast<void*>(const_cast< Job*>(this));
    return QObject::qt_metacast(_clname);
}

int QKeychain::Job::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void QKeychain::Job::finished(QKeychain::Job * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
struct qt_meta_stringdata_QKeychain__ReadPasswordJob_t {
    QByteArrayData data[1];
    char stringdata0[27];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QKeychain__ReadPasswordJob_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QKeychain__ReadPasswordJob_t qt_meta_stringdata_QKeychain__ReadPasswordJob = {
    {
QT_MOC_LITERAL(0, 0, 26) // "QKeychain::ReadPasswordJob"

    },
    "QKeychain::ReadPasswordJob"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QKeychain__ReadPasswordJob[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QKeychain::ReadPasswordJob::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject QKeychain::ReadPasswordJob::staticMetaObject = {
    { &Job::staticMetaObject, qt_meta_stringdata_QKeychain__ReadPasswordJob.data,
      qt_meta_data_QKeychain__ReadPasswordJob,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QKeychain::ReadPasswordJob::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QKeychain::ReadPasswordJob::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QKeychain__ReadPasswordJob.stringdata0))
        return static_cast<void*>(const_cast< ReadPasswordJob*>(this));
    return Job::qt_metacast(_clname);
}

int QKeychain::ReadPasswordJob::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Job::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_QKeychain__WritePasswordJob_t {
    QByteArrayData data[1];
    char stringdata0[28];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QKeychain__WritePasswordJob_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QKeychain__WritePasswordJob_t qt_meta_stringdata_QKeychain__WritePasswordJob = {
    {
QT_MOC_LITERAL(0, 0, 27) // "QKeychain::WritePasswordJob"

    },
    "QKeychain::WritePasswordJob"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QKeychain__WritePasswordJob[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QKeychain::WritePasswordJob::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject QKeychain::WritePasswordJob::staticMetaObject = {
    { &Job::staticMetaObject, qt_meta_stringdata_QKeychain__WritePasswordJob.data,
      qt_meta_data_QKeychain__WritePasswordJob,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QKeychain::WritePasswordJob::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QKeychain::WritePasswordJob::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QKeychain__WritePasswordJob.stringdata0))
        return static_cast<void*>(const_cast< WritePasswordJob*>(this));
    return Job::qt_metacast(_clname);
}

int QKeychain::WritePasswordJob::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Job::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_QKeychain__DeletePasswordJob_t {
    QByteArrayData data[1];
    char stringdata0[29];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QKeychain__DeletePasswordJob_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QKeychain__DeletePasswordJob_t qt_meta_stringdata_QKeychain__DeletePasswordJob = {
    {
QT_MOC_LITERAL(0, 0, 28) // "QKeychain::DeletePasswordJob"

    },
    "QKeychain::DeletePasswordJob"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QKeychain__DeletePasswordJob[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QKeychain::DeletePasswordJob::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject QKeychain::DeletePasswordJob::staticMetaObject = {
    { &Job::staticMetaObject, qt_meta_stringdata_QKeychain__DeletePasswordJob.data,
      qt_meta_data_QKeychain__DeletePasswordJob,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QKeychain::DeletePasswordJob::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QKeychain::DeletePasswordJob::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QKeychain__DeletePasswordJob.stringdata0))
        return static_cast<void*>(const_cast< DeletePasswordJob*>(this));
    return Job::qt_metacast(_clname);
}

int QKeychain::DeletePasswordJob::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Job::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
