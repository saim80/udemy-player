/****************************************************************************
** Meta object code from reading C++ file 'signinform.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "signinform.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'signinform.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_SignInForm_t {
    QByteArrayData data[9];
    char stringdata0[195];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SignInForm_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SignInForm_t qt_meta_stringdata_SignInForm = {
    {
QT_MOC_LITERAL(0, 0, 10), // "SignInForm"
QT_MOC_LITERAL(1, 11, 31), // "on_emailTextfield_returnPressed"
QT_MOC_LITERAL(2, 43, 0), // ""
QT_MOC_LITERAL(3, 44, 34), // "on_passwordTextfield_returnPr..."
QT_MOC_LITERAL(4, 79, 27), // "on_rememberCheckbox_toggled"
QT_MOC_LITERAL(5, 107, 7), // "checked"
QT_MOC_LITERAL(6, 115, 23), // "on_signInButton_clicked"
QT_MOC_LITERAL(7, 139, 23), // "on_signUpButton_clicked"
QT_MOC_LITERAL(8, 163, 31) // "on_forgotPasswordButton_clicked"

    },
    "SignInForm\0on_emailTextfield_returnPressed\0"
    "\0on_passwordTextfield_returnPressed\0"
    "on_rememberCheckbox_toggled\0checked\0"
    "on_signInButton_clicked\0on_signUpButton_clicked\0"
    "on_forgotPasswordButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SignInForm[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x08 /* Private */,
       3,    0,   45,    2, 0x08 /* Private */,
       4,    1,   46,    2, 0x08 /* Private */,
       6,    0,   49,    2, 0x08 /* Private */,
       7,    0,   50,    2, 0x08 /* Private */,
       8,    0,   51,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void SignInForm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SignInForm *_t = static_cast<SignInForm *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_emailTextfield_returnPressed(); break;
        case 1: _t->on_passwordTextfield_returnPressed(); break;
        case 2: _t->on_rememberCheckbox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->on_signInButton_clicked(); break;
        case 4: _t->on_signUpButton_clicked(); break;
        case 5: _t->on_forgotPasswordButton_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject SignInForm::staticMetaObject = {
    { &Window::staticMetaObject, qt_meta_stringdata_SignInForm.data,
      qt_meta_data_SignInForm,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *SignInForm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SignInForm::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_SignInForm.stringdata0))
        return static_cast<void*>(const_cast< SignInForm*>(this));
    return Window::qt_metacast(_clname);
}

int SignInForm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Window::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
