#include "paginateddata.h"


const int PaginatedData::DefaultPage     = 1;
const int PaginatedData::DefaultPageSize = 16;

PaginatedData::PaginatedData()
{
    reset();
}

void PaginatedData::reset()
{
    _page = 0;
    nextURL = "";
}


void PaginatedData::loadNext(UdemyAPI::JSONCompletion completion)
{
    _page++;
    load(_page, DefaultPageSize, completion);
}