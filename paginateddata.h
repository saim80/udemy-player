#ifndef PAGINATEDDATA_H
#define PAGINATEDDATA_H

#include <QString>
#include "udemyapiaccessmanager.h"

class PaginatedData {
public:
    PaginatedData();
    static const int DefaultPage;
    static const int DefaultPageSize;
    
    virtual void reset();
    virtual void loadNext(UdemyAPI::JSONCompletion completion = nullptr);
    
    inline bool empty() { return _page == 0; };
    inline bool hasMorePages() { return nextURL.size() > 0; };
protected:
    virtual void load(const int& page=DefaultPage, const int& pageSize=DefaultPageSize,
                      UdemyAPI::JSONCompletion completion = nullptr) = 0;
    
    QString nextURL;
private:
    int _page;
};


#endif // PAGINATEDDATA_H