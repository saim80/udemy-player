#include "passwordvalidator.h"

PasswordValidator::PasswordValidator(const uint& minLength, const uint& maxLength, QObject *parent) :
    QValidator(parent), _minLength(minLength), _maxLength(maxLength)
{

}

QValidator::State PasswordValidator::validate(QString &input, int &pos) const
{
    const auto stringInRange = input.right(input.size() - pos);
    const auto size = (uint)stringInRange.size();

    const auto success = QValidator::State::Acceptable;
    const auto fail    = QValidator::State::Invalid;

    return size >= _minLength && size <= _maxLength ? success : fail;
}
