#ifndef PASSWORDVALIDATOR_H
#define PASSWORDVALIDATOR_H

#include <QValidator>

class PasswordValidator : public QValidator
{
    Q_OBJECT
public:
    explicit PasswordValidator(
            const uint &minLength,
            const uint &maxLength,
            QObject *parent = 0);

    QValidator::State validate(QString &, int &) const Q_DECL_OVERRIDE;

private:
    uint _minLength;
    uint _maxLength;
signals:

public slots:
};

#endif // PASSWORDVALIDATOR_H
