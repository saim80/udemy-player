#include "settings.h"

#include "udcred.h"
#include "keychain.h"

const char *Settings::Host            = "www.udemy.com";
const char *Settings::APIPrefix       = "api-2.0";
const char *Settings::ClientUserAgent = "Mozilla/5.0 (%1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36 %2/%3";

const char *Settings::KeyAccessToken      = "access_token";
const char *Settings::KeyRememberPassword = "remember_password";
const char *Settings::KeyClientID         = "client_key";
const char *Settings::KeyClientSecret     = "client_secret";
const char *Settings::KeyUserAgent        = "user_agent";

const char *PlatformWindows = "Windows";
const char *PlatformMac     = "Macintosh";

const char *Protocol = "https";

static Settings __shared;

Settings::Settings(QObject *parent) : QObject(parent)
{
    _insecureSettings = new QSettings("Udemy", getAppTitle());
    _tempValues       = new QVariantMap();
    
    auto env = QProcessEnvironment::systemEnvironment();
    
    tempSet(KeyClientID, QString::fromStdString(udcred::get_udemy_client_key()));
    tempSet(KeyClientSecret, QString::fromStdString(udcred::get_udemy_client_secret()));
    
#ifdef Q_OS_MAC
    tempSet(KeyUserAgent, QString(ClientUserAgent).arg(PlatformMac, getAppTitle(), getAppVersion()));
#else
    tempSet(KeyUserAgent, QString(ClientUserAgent).arg(PlatformWindows, getAppTitle(), getAppVersion()));
#endif

}

Settings::~Settings()
{
    delete _insecureSettings;
    delete _tempValues;
}

Settings &Settings::sharedSettings()
{
   return __shared;
}

QUrl Settings::getBaseUrl()
{
    const auto &aUrl = QString("%1://%2").arg(Protocol, Host);

    return aUrl;
}

QUrl Settings::getUrl(const class QString &path, const class QString &query)
{
    const auto &aUrl = QString("%1/%2").arg(getBaseUrl().toString(), path);
    auto outUrl = QUrl(aUrl);

    outUrl.setQuery(query);

    return outUrl;
}

QUrl Settings::getAPIUrl(const class QString &path, const class QString &query)
{
    const auto &aUrl = QString("%1/%2/%3").arg(getBaseUrl().toString(), APIPrefix, path);
    auto outUrl = QUrl(aUrl);

    outUrl.setQuery(query);

    return outUrl;
}

QString Settings::getAppTitle()
{
    return QTranslator::tr("UDPlayer");
}

QString Settings::getAppVersion()
{
    return "1.0";
}

void Settings::configureNetworkRequest(class QNetworkRequest &request)
{
    const auto &agentString = get(KeyUserAgent).toByteArray();
    const auto &accessToken = tempGet("access_token").toString();
    const auto &clientKey   = tempGet(KeyClientID).toString();
    const auto &clientSec   = tempGet(KeyClientSecret).toString();
    const auto &visitHash   = tempGet("visitHash").toString();
    const auto &visitorHash = tempGet("visitorHash").toString();

    QString authValue = "";

    request.setRawHeader("User-Agent", agentString);
    request.setRawHeader("Content-Type", "application/json");
    request.setRawHeader("X-Client-Name", getAppTitle().toUtf8());
    request.setRawHeader("X-Version-Name", getAppVersion().toUtf8());
    request.setRawHeader("X-Udemy-Client-Id", clientKey.toUtf8());
    request.setRawHeader("X-Udemy-Client-Secret", clientSec.toUtf8());
    request.setRawHeader("X-Mobile-Visit-Enabled", QString("true").toUtf8());

    if (accessToken.length() > 0)
    {
        authValue = QString("Bearer %1").arg(accessToken);

        qDebug() << "User Auth: " << authValue;
    }
    else
    {
        auto tokenString    = QString("%1:%2").arg(clientKey, clientSec);
        auto anonymousToken = tokenString.toUtf8().toBase64();
        
        authValue = QString("Basic %2").arg(QString(anonymousToken));

        qDebug() << "Basic Auth: " << authValue;
    }

    request.setRawHeader("Authorization", authValue.toUtf8());
    
    if (visitHash.size() > 0 && visitorHash.size() > 0) {
        auto cookieString = QString("__udmyvst=%1; __udmyvstr=%2").arg(visitHash, visitorHash);
        request.setRawHeader("Cookie", cookieString.toUtf8());
    }
}

void Settings::secureSet(const class QString &key, const class QVariant &value, std::function<void()> completion)
{
    auto writeJob = new QKeychain::WritePasswordJob(getAppTitle());

    writeJob->setAutoDelete(true);
    writeJob->setKey(key);

    QObject::connect(writeJob, &QKeychain::WritePasswordJob::finished, [=](QKeychain::Job *job) {
        if (completion)
        {
            auto passJob = qobject_cast<QKeychain::WritePasswordJob *>(job);

            if (passJob == writeJob)
            {
                completion();
            }
        }
    });

    writeJob->setBinaryData(value.toByteArray());
    writeJob->start();
}

void Settings::secureGet(const class QString &key, std::function<void(const class QVariant&)> completion)
{
    auto readJob = new QKeychain::ReadPasswordJob(getAppTitle());

    readJob->setAutoDelete(true);
    readJob->setKey(key);

    QObject::connect(readJob, &QKeychain::ReadPasswordJob::finished, [=](QKeychain::Job *job) {
        if (completion)
        {
            auto passJob = qobject_cast<QKeychain::ReadPasswordJob *>(job);

            if (passJob == readJob)
            {
                completion(QVariant(passJob->binaryData()));
            }
        }
    });

    readJob->start();
}

void Settings::set(const class QString &key, const class QVariant &value)
{
    auto &settings = sharedSettings();

    settings._insecureSettings->setValue(key, value);
    settings._insecureSettings->sync();
}

class QVariant Settings::get(const class QString &key)
{
    auto &settings = sharedSettings();

    return settings._insecureSettings->value(key);
}

void Settings::tempSet(const class QString &key, const class QVariant &value)
{
    sharedSettings()._tempValues->insert(key, value);
}

class QVariant Settings::tempGet(const class QString &key)
{
    return sharedSettings()._tempValues->value(key);
}

