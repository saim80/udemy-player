#ifndef SETTINGS_H
#define SETTINGS_H

#include <functional>

#include <QtCore>
#include <QtNetwork>

class Settings : public QObject
{
    Q_OBJECT

public:
    explicit Settings(QObject *parent = 0);
    ~Settings();

    static Settings &sharedSettings();

    static const char *Host;
    static const char *APIPrefix;
    static const char *ClientUserAgent;

    static const char *KeyAccessToken;
    static const char *KeyRememberPassword;
    static const char *KeyClientID;
    static const char *KeyClientSecret;
    static const char *KeyUserAgent;

    static QUrl getBaseUrl();
    static QUrl getUrl(const QString &path, const QString &query = QString());
    static QUrl getAPIUrl(const QString &path, const QString &query = QString());

    static QString getAppTitle();
    static QString getAppVersion();

    static void configureNetworkRequest(QNetworkRequest &request);
signals:

public slots:

// Disk persistance.
public:
    static void set(const QString &key, const QVariant &value);
    static QVariant get(const QString &key);
private:
    class QSettings *_insecureSettings;

// In memory persistence.
public:
    static void tempSet(const QString &key, const QVariant &value);
    static QVariant tempGet(const QString &key);
private:
    QVariantMap *_tempValues;

// Secured persistence.
public:
    static void secureSet(const QString &key, const QVariant &value, std::function< void() > completion = nullptr);
    static void secureGet(const QString &key, std::function<void(const QVariant&)> completion = nullptr);
};

#endif // SETTINGS_H
