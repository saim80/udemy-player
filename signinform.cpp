#include "signinform.h"
#include "ui_signinform.h"

#include "settings.h"
#include "udemyapiaccessmanager.h"
#include "emailvalidator.h"
#include "passwordvalidator.h"

#include <QMessageBox>
#include <QtDebug>
#include <QObject>

#include "hoverbutton.h"
#include "coursebrowser.h"
#include "waitingspinnerwidget.h"

const char* SignInForm::KeyEmail    = "email";
const char* SignInForm::KeyPassword = "password";

SignInForm::SignInForm(QWidget *parent) :
    Window(parent),
    ui(new Ui::SignInForm),
    _spinner(new WaitingSpinnerWidget(this))
{
    auto const &rememberPass = Settings::get(Settings::KeyRememberPassword).toBool();
    
    ui->setupUi(this);
    ui->rememberCheckbox->setChecked(rememberPass);
    
    connect(ui->bar->closeButton, &HoverButton::clicked, [=]() {
        QApplication::quit();
    });
    
    _configureValidators();
    _autoLoginInNecessary();
}

void SignInForm::_configureValidators()
{
    _emailValidator    = new EmailValidator(this);
    _passwordValidator = new PasswordValidator(1, 128, this);
}

void SignInForm::_autoLoginInNecessary()
{
    _lockUI("");
    
    Settings::secureGet(KeyEmail, [=](const QVariant &email) {
        ui->emailTextfield->setText(email.toString());
        Settings::secureGet(KeyPassword, [=](const QVariant &pass) {
            ui->passwordTextfield->setText(pass.toString());
            
            this->_unlockUI();
            this->on_signInButton_clicked();
        });
    });
}

SignInForm::~SignInForm()
{
    delete ui;
}

void SignInForm::on_emailTextfield_returnPressed()
{
   QLineEdit * const &emailField = ui->emailTextfield;
   QLineEdit * const &passField  = ui->passwordTextfield;

   if (emailField && passField)
   {
       _validateEmailAddress([&] (const bool &passed, const QString &message)
       {
           if (passed)
           {
               passField->setFocus();
           }
           else
           {
               emailField->clear();
           }
           emailField->setPlaceholderText(message);
       });
   }
}

void SignInForm::on_passwordTextfield_returnPressed()
{
    QLineEdit   * const &passField    = ui->passwordTextfield;
    QPushButton * const &signInButton = ui->signInButton;

    if (passField && signInButton) {
        _validatePassword([&] (const bool &passed, const QString &message)
        {
            if (passed)
            {
                on_signInButton_clicked();
            }
            else
            {
                passField->clear();
            }
            passField->setPlaceholderText(message);
        });
    }
}

void SignInForm::on_rememberCheckbox_toggled(bool checked)
{
    Settings::set(Settings::KeyRememberPassword, QVariant::fromValue(checked));
}

void SignInForm::_validateEmailAddress(AfterValidation completion)
{
    QString errorMessage = "";

    auto strEmailAddr = ui->emailTextfield->text();
    auto pos          = 0;

    bool result = _emailValidator->validate(strEmailAddr, pos) == QValidator::Acceptable;

    if (!result)
    {
        errorMessage = tr("Invalid email address");
    }
    else
    {
        errorMessage = tr("Email address");
    }

    completion(result, errorMessage);
}

void SignInForm::_validatePassword(AfterValidation completion)
{
    QString errorMessage = "";
    bool result = false;

    auto strPassword = ui->passwordTextfield->text();
    auto pos         = 0;

    result       = _passwordValidator->validate(strPassword, pos) == QValidator::Acceptable;
    errorMessage = tr("Password");

    completion(result, errorMessage);
}

void SignInForm::_lockUI(const QString &message)
{
    _spinner->start();
    ui->messageLabel->setText(message);
}

void SignInForm::_unlockUI()
{
    _spinner->stop();
    ui->messageLabel->setText("");
}

void SignInForm::on_signInButton_clicked()
{
    const auto &emailText = ui->emailTextfield->text();
    const auto &passText  = ui->passwordTextfield->text();

    if (emailText.length() > 0 && passText.length() > 0)
    {
        _doSignIn(emailText, passText);
    }
}

void SignInForm::_doSignIn(const QString &emailText, const QString &passText)
{
    auto params = QVariantMap();
    auto apiAccess = qobject_cast<UdemyAPIAccessManager*>(netManager);

    params[KeyEmail]    = emailText;
    params[KeyPassword] = passText;

    auto reply = apiAccess->post("auth/udemy-auth/login/", params);

    _lockUI(tr("Signing in..."));

    connect(reply, &QNetworkReply::finished, [=]() {
        const auto &jsonDoc = QJsonDocument::fromJson(reply->readAll());
        const auto &map = jsonDoc.toVariant().toMap();

        qDebug() << "jsonDoc" << jsonDoc.toJson();

        if (map.value(Settings::KeyAccessToken).toString().size() > 0)
        {
            for (auto it = map.constBegin(); it != map.constEnd(); it++)
            {
                Settings::tempSet(it.key(), it.value());
            }

            if (Settings::get(Settings::KeyRememberPassword).toBool())
            {
                Settings::secureSet(KeyEmail, emailText);
                Settings::secureSet(KeyPassword, passText);
            }

            close();

            auto browser = new CourseBrowser();

            browser->show();
        }
        else
        {
            ui->passwordTextfield->clear();
            ui->emailTextfield->clear();

            Settings::secureSet(KeyEmail, "");
            Settings::secureSet(KeyPassword, "");

            QMessageBox msgBox;

            msgBox.setStyleSheet("background-color:white;");
            msgBox.information(nullptr,
                              tr("Sign In"),
                              tr("Failed. Please check your credentials and network connection."));
        }

        this->_unlockUI();
        reply->deleteLater();
    });
}

void SignInForm::on_signUpButton_clicked()
{
    const auto &aUrl = Settings::getBaseUrl();

    QDesktopServices::openUrl(aUrl);
}

void SignInForm::on_forgotPasswordButton_clicked()
{
    const auto &aUrl = Settings::getUrl("user/forgot-password");

    QDesktopServices::openUrl(aUrl);
}
