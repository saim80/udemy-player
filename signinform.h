#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <functional>
#include <unordered_set>

#include "window.h"

namespace Ui {
class SignInForm;
}

class SignInForm : public Window
{
    Q_OBJECT

public:
    static const char* KeyEmail;
    static const char* KeyPassword;

    explicit SignInForm(QWidget *parent = 0);
    ~SignInForm();

private slots:
    void on_emailTextfield_returnPressed();
    void on_passwordTextfield_returnPressed();
    void on_rememberCheckbox_toggled(bool checked);
    void on_signInButton_clicked();
    void on_signUpButton_clicked();

    void on_forgotPasswordButton_clicked();

private:
    Ui::SignInForm *ui;
    void _configureValidators();
    void _autoLoginInNecessary();

private:
    typedef const std::function<void(const bool &,const class QString &)> AfterValidation;

    class EmailValidator *_emailValidator;
    class PasswordValidator *_passwordValidator;
    void _validateEmailAddress(AfterValidation completion);
    void _validatePassword(AfterValidation completion);

private:
    class WaitingSpinnerWidget* _spinner;
    void _lockUI(const QString &message);
    void _unlockUI();
    void _doSignIn(const QString &emailText, const QString &passText);
};

#endif // MAINWINDOW_H
