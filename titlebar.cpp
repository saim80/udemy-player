#include "titlebar.h"
#include "hoverbutton.h"

#include <QHBoxLayout>
#include <QSpacerItem>

const int TitleBar::Height = 32;

TitleBar::TitleBar(QWidget *parent) : QWidget(parent), closeButton(new HoverButton(this))
{
    const auto hlayout = new QHBoxLayout(this);
    
    setLayout(hlayout);
    configureCloseButton();
}

void TitleBar::configureCloseButton()
{
    auto hlayout = qobject_cast<QHBoxLayout*>(layout());
    
    closeButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    closeButton->setFixedSize(Height, Height);
    
#ifdef Q_OS_MAC
    closeButton->setHoverImagePath(":/mac/mac-close-hover.png");
    closeButton->setNormalImagePath(":/mac/mac-close.png");
    
    hlayout->addWidget(closeButton);
    hlayout->addStretch();
#else
    closeButton->setHoverImagePath(":/windows/windows-close.png");
    closeButton->setNormalImagePath(":/windows/windows-close-hover.png");
    
    hlayout->addStretch();
    hlayout->addWidget(closeButton);
#endif
    
    hlayout->setMargin(1);
}
