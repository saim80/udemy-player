#ifndef TITLEBAR_H
#define TITLEBAR_H

#include <QWidget>

class TitleBar : public QWidget
{
    Q_OBJECT
public:
    explicit TitleBar(QWidget *parent = 0);
    
    static const int Height;
    
    class HoverButton *closeButton;
    
protected:
    virtual void configureCloseButton();
private:
signals:

public slots:
};

#endif // TITLEBAR_H
