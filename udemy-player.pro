#-------------------------------------------------
#
# Project created by QtCreator 2016-03-12T11:51:50
#
#-------------------------------------------------

QT       += core gui network dbus webkit webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = udemy-player
TEMPLATE = app

DEFINES += QKEYCHAIN_STATICLIB
DEPENDPATH += $$PWD/Libraries
INCLUDEPATH += $$PWD/Libraries/udcred/include $$PWD/Libraries/qt-keychain $$PWD/Libraries/qt-spinner

SOURCES += main.cpp\
           hoverbutton.cpp \
           settings.cpp \
           udemyapiaccessmanager.cpp \
           Libraries/qt-keychain/keychain.cpp \
    coursebrowser.cpp \
    signinform.cpp \
    utils.cpp \
    emailvalidator.cpp \
    passwordvalidator.cpp \
    window.cpp \
    courselist.cpp \
    titlebar.cpp \
    Libraries/qt-spinner/waitingspinnerwidget.cpp \
    paginateddata.cpp \
    coursebrowsertitlebar.cpp

macx {
    QT      += macextras
    SOURCES += Libraries/qt-keychain/keychain_mac.cpp
    QMAKE_LFLAGS += -F /System/Library/Frameworks/CoreFoundation.framework/
    QMAKE_LFLAGS += -F /System/Library/Frameworks/Security.framework/
    LIBS += -framework CoreFoundation -framework Security
    LIBS += -L$$PWD/Libraries/udcred/lib -ludcred
    QMAKE_MACOSX_DEPLOYMENT_TARGET=10.11
}
win32 {
    QT      += winextras
    SOURCES += Libraries/qt-keychain/keychain_win.cpp
    LIBS    += -L$$PWD/Libraries/udcred/lib -ludcred
    LIBS    += -lcrypt32
}

#unix:!macx {
#    QT      += x11extras
#    SOURCES += Libraries/qt-keychain/gnomekeyring.cpp
#    HEADERS += Libraries/qt-keychain/gnomekeyring_p.h
#}

HEADERS  += \
    Libraries/qt-keychain/keychain.h \
    Libraries/qt-keychain/keychain_p.h \
    Libraries/qt-keychain/qkeychain_export.h \
    hoverbutton.h \
    settings.h \
    udemyapiaccessmanager.h \
    coursebrowser.h \
    signinform.h \
    utils.h \
    emailvalidator.h \
    passwordvalidator.h \
    window.h \
    courselist.h \
    titlebar.h \
    Libraries/qt-spinner/waitingspinnerwidget.h \
    paginateddata.h \
    coursebrowsertitlebar.h

FORMS    += \
    signinform.ui \
    coursebrowser.ui

DISTFILES +=

RESOURCES += \
    images.qrc

CONFIG += c++11
