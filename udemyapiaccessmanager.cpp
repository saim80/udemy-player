#include "udemyapiaccessmanager.h"

#include <QWidget>
#include <QString>
#include <QMap>
#include <QUrlQuery>
#include <QByteArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QtDebug>

#include "settings.h"

UdemyAPIAccessManager::UdemyAPIAccessManager(QWidget *parent) : QNetworkAccessManager(parent)
{

}

QNetworkReply* UdemyAPIAccessManager::get(const QString &path, const QVariantMap& params)
{
    auto request = QNetworkRequest(Settings::getAPIUrl(path, getQuery(params)));
    
    configureNetworkRequest(request);
    
    qDebug() << request.url().toString() ;
    return QNetworkAccessManager::get(request);
}

QNetworkReply* UdemyAPIAccessManager::post(const QString &path, const QVariantMap& params)
{
    auto request = QNetworkRequest(Settings::getAPIUrl(path, getQuery(params)));
    
    configureNetworkRequest(request);
    
    const auto body = getPostBody(params);

    if (body.size() > 0)
    {
        request.setRawHeader("Content-Length", QVariant(body.size()).toByteArray());
    }

    return QNetworkAccessManager::post(request, body);
}
QNetworkReply* UdemyAPIAccessManager::head(const QString &path, const QVariantMap& params)
{
    auto request = QNetworkRequest(Settings::getAPIUrl(path, getQuery(params)));
    
    configureNetworkRequest(request);
    
    return QNetworkAccessManager::head(request);
}

QNetworkReply* UdemyAPIAccessManager::put(const QString &path, const QVariantMap& params)
{
    auto request = QNetworkRequest(Settings::getAPIUrl(path, getQuery(params)));

    const auto body = getPostBody(params);
    
    configureNetworkRequest(request);
    
    if (body.size() > 0)
    {
        request.setRawHeader("Content-Length", QVariant(body.size()).toByteArray());
    }

    return QNetworkAccessManager::put(request, body);
}

QNetworkReply* UdemyAPIAccessManager::deleteResource(const QString &path, const QVariantMap& params)
{
    auto request = QNetworkRequest(Settings::getAPIUrl(path, getQuery(params)));
    
    configureNetworkRequest(request);
    
    return QNetworkAccessManager::deleteResource(request);
}

QString UdemyAPIAccessManager::getQuery(const QVariantMap& params)
{
    auto query = QUrlQuery();

    for (auto it = params.constBegin(); it != params.constEnd(); it++)
    {
        query.addQueryItem(it.key(), it.value().toString());
    }

    return query.toString();
}

QByteArray UdemyAPIAccessManager::getPostBody(const QVariantMap& params)
{
    const auto &json = QJsonObject::fromVariantMap(params);
    const auto &doc  = QJsonDocument(json);

    return doc.toJson();
}

void UdemyAPIAccessManager::configureNetworkRequest(QNetworkRequest &request)
{
    Settings::configureNetworkRequest(request);

    request.setRawHeader("Content-Type", "application/json");
}

//--------------------------------------------------------------------------------------------------------------------------------

UdemyWebAccessManager::UdemyWebAccessManager(QWidget *parent) : QNetworkAccessManager(parent) {};


QNetworkReply *UdemyWebAccessManager::createRequest(QNetworkAccessManager::Operation op, const QNetworkRequest &request,
                                                    QIODevice *outgoingData)
{
    auto &mutableRequest = (QNetworkRequest&)request;
    
    const auto &agentString = Settings::get(Settings::KeyUserAgent).toByteArray();
    const auto &accessToken = Settings::tempGet("access_token").toString();
    const auto &clientKey   = Settings::tempGet(Settings::KeyClientID).toString();
    const auto &clientSec   = Settings::tempGet(Settings::KeyClientSecret).toString();
    
    QString authValue = "";
    
    mutableRequest.setRawHeader("User-Agent", agentString);
    
    if (accessToken.length() > 0)
    {
        authValue = QString("Bearer %1").arg(accessToken);
        
        qDebug() << "User Auth: " << authValue;
    }
    else
    {
        auto tokenString    = QString("%1:%2").arg(clientKey, clientSec);
        auto anonymousToken = tokenString.toUtf8().toBase64();
        
        authValue = QString("Basic %2").arg(QString(anonymousToken));
        
        qDebug() << "Basic Auth: " << authValue;
    }
    
    mutableRequest.setRawHeader("X-Udemy-Client-Id", clientKey.toUtf8());
    mutableRequest.setRawHeader("X-Udemy-Client-Secret", clientSec.toUtf8());
    mutableRequest.setRawHeader("Authorization", authValue.toUtf8());
    mutableRequest.setRawHeader("X-Udemy-Authorization", authValue.toUtf8());
    
    auto reply = QNetworkAccessManager::createRequest(op, mutableRequest, outgoingData);
    return reply;
}