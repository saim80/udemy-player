#ifndef UDEMYAPIACCESSMANAGER_H
#define UDEMYAPIACCESSMANAGER_H

#include <functional>

#include <QJsonDocument>
#include <QtNetwork>

namespace UdemyAPI {
typedef std::function<void (const QJsonDocument&)> JSONCompletion;
}

class UdemyAPIAccessManager : public QNetworkAccessManager
{
    Q_OBJECT
public:
    UdemyAPIAccessManager(class QWidget *parent);

    QNetworkReply* get(const QString &path, const QVariantMap& params);
    QNetworkReply* head(const QString &path, const QVariantMap& params);
    QNetworkReply* post(const QString &path, const QVariantMap& params);
    QNetworkReply* put(const QString &path, const QVariantMap& params);
    QNetworkReply* deleteResource(const QString &path, const QVariantMap& params);
    
private:
    QString getQuery(const QVariantMap& params);
    QByteArray getPostBody(const QVariantMap& params);

    void configureNetworkRequest(QNetworkRequest &request);
};

class UdemyWebAccessManager : public QNetworkAccessManager
{
    Q_OBJECT
public:
    UdemyWebAccessManager(class QWidget *parent);
    virtual QNetworkReply *createRequest(QNetworkAccessManager::Operation op, const QNetworkRequest &request,
                                         QIODevice *outgoingData = 0) override;
};

#endif // UDEMYAPIACCESSMANAGER_H
