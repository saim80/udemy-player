#include "window.h"
#include "udemyapiaccessmanager.h"

Window::Window(QWidget *parent) :
    QMainWindow(parent),
    netManager(new UdemyAPIAccessManager(this)),
    resizeEnabled(false),
    resizeWidth(5)
{
    setWindowFlags(Qt::Window|Qt::FramelessWindowHint);
    setAttribute(Qt::WA_NoSystemBackground, true);
    setAttribute(Qt::WA_TranslucentBackground, true);

    netManager->connectToHostEncrypted("https://www.udemy.com");
}

void Window::setDropShadowEnabled(const bool &enabled)
{
    if (enabled != dropShadowEnabled())
    {
        const Qt::WindowFlags defaultValues = Qt::Window | Qt::FramelessWindowHint;
        
        if (enabled)
        {
            setWindowFlags(defaultValues);
        }
        else
        {
            setWindowFlags(defaultValues | Qt::NoDropShadowWindowHint);
        }
    }
}

bool Window::dropShadowEnabled()
{
    return !(windowFlags() & Qt::NoDropShadowWindowHint);
}

void Window::mousePressEvent(QMouseEvent *event)
{
    QMainWindow::mousePressEvent(event);
    
    _mouseClick = QCursor::pos();
    _mouseClickRect = geometry();
}

void Window::mouseMoveEvent(QMouseEvent *event)
{
    QMainWindow::mouseMoveEvent(event);
    
    if (_mouseClick != QPoint())
    {
        auto newPos = QCursor::pos();
        auto oldGeometry = geometry();
        auto newGeometry = oldGeometry;
        
        if (resizeActiveHandleMask != None) {
            QMargins margins;
            
            if ((resizeActiveHandleMask & Left))
            {
                margins.setLeft(newPos.x() - _mouseClick.x());
            }
            if ((resizeActiveHandleMask & Right))
            {
                margins.setRight(_mouseClick.x() - newPos.x());
            }
            if ((resizeActiveHandleMask & Top))
            {
                margins.setTop(newPos.y() - _mouseClick.y());
            }
            if ((resizeActiveHandleMask & Bottom))
            {
                margins.setBottom(_mouseClick.y() - newPos.y());
            }
            
            newGeometry = _mouseClickRect - margins;
        } else {
            newGeometry.moveTopLeft(QPoint(_mouseClickRect.left() + newPos.x() - _mouseClick.x(),
                                           _mouseClickRect.top()  + newPos.y() - _mouseClick.y()));
        }
        
        setGeometry(newGeometry);
        
        if (newGeometry.size() != geometry().size())
        {
            setGeometry(oldGeometry);
        }
    } else {
        updateCursor();
    }
}

void Window::enterEvent(class QEvent *event)
{
    QMainWindow::enterEvent(event);
    updateCursor();
}
void Window::leaveEvent(class QEvent *event)
{
    QMainWindow::leaveEvent(event);
    updateCursor();
}

void Window::mouseReleaseEvent(QMouseEvent *event)
{
    QMainWindow::mouseReleaseEvent(event);
    
    _mouseClick = QPoint();
}

void Window::updateCursor()
{
    if (!resizeEnabled || _mouseClick != QPoint())
    {
        return;
    }
    
    const auto &frame     = frameGeometry();
    const auto leftEdge   = frame - QMargins(0, 0, frame.width() - resizeWidth, 0);
    const auto topEdge    = frame - QMargins(0, 0, 0, frame.height() - resizeWidth);
    const auto rightEdge  = frame - QMargins(frame.width() - resizeWidth, 0, 0, 0);
    const auto bottomEdge = frame - QMargins(0, frame.height() - resizeWidth, 0, 0);
    
    const auto& pos = QCursor::pos();
    
    if (leftEdge.contains(pos) && topEdge.contains(pos))
    {
        resizeActiveHandleMask = Left | Top;
        setCursor(Qt::SizeFDiagCursor);
    }
    else if (rightEdge.contains(pos) && bottomEdge.contains(pos))
    {
        resizeActiveHandleMask = Right | Bottom;
        setCursor(Qt::SizeFDiagCursor);
    }
    else if (leftEdge.contains(pos) && bottomEdge.contains(pos))
    {
        resizeActiveHandleMask = Left | Bottom;
        setCursor(Qt::SizeBDiagCursor);
    }
    else if (rightEdge.contains(pos) && topEdge.contains(pos))
    {
        resizeActiveHandleMask = Right | Top;
        setCursor(Qt::SizeBDiagCursor);
    }
    else if (topEdge.contains(pos))
    {
        resizeActiveHandleMask = Top;
        setCursor(Qt::SizeVerCursor);
    }
    else if (bottomEdge.contains(pos))
    {
        resizeActiveHandleMask = Bottom;
        setCursor(Qt::SizeVerCursor);
    }
    else if (leftEdge.contains(pos))
    {
        resizeActiveHandleMask = Left;
        setCursor(Qt::SizeHorCursor);
    }
    else if (rightEdge.contains(pos))
    {
        resizeActiveHandleMask = Right;
        setCursor(Qt::SizeHorCursor);
    }
    else
    {
        resizeActiveHandleMask = None;
        setCursor(Qt::ArrowCursor);
    }
}
