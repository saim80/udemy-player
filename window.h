#ifndef WINDOW_H
#define WINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>

class Window : public QMainWindow
{
    Q_OBJECT
public:
    explicit Window(QWidget *parent = 0);

protected:
    void enterEvent(class QEvent *event) override;
    void leaveEvent(class QEvent *event) override;
    void mousePressEvent(class QMouseEvent *event) override;
    void mouseMoveEvent(class QMouseEvent *event) override;
    void mouseReleaseEvent(class QMouseEvent *event) override;

private:
    QPoint _mouseClick;

protected:
    class QNetworkAccessManager *netManager;

    // Resize support.
public:
    inline void setCanResize(const bool& resizable) { resizeEnabled = resizable; };
    inline bool canResize() { return resizeEnabled; };
    inline int resizeHandleWidth() { return resizeWidth; };
    inline void setResizeHandleWidth(const int& width) { resizeWidth = width; };
    void setDropShadowEnabled(const bool& enabled);
    bool dropShadowEnabled();
protected:
    enum ResizeEdges_ : int {
        None = 0,
        Top = 1,
        Left = 1 << 1,
        Bottom = 1 << 2,
        Right = 1 << 3
    };
    typedef int ResizeEdges;
    
    bool resizeEnabled;
    int resizeWidth;
    ResizeEdges resizeActiveHandleMask;
    virtual void updateCursor();
    
private:
    QRect _mouseClickRect;
    
signals:

public slots:
};

#endif // WINDOW_H
